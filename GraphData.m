function GraphData
MakeData
fid=fopen('~/Desktop/graphedata.tex','wt');
fprintf(fid,'%s\n','\tiny');

minigraphdata(fid,dY,'$\Delta \log Y$');
minigraphdata(fid,dC,'$\Delta \log C$');
minigraphdata(fid,dI,'$\Delta \log I$');
minigraphdata(fid,dW,'$\Delta \log \left( w L\right)$');
fprintf(fid,'%s\n','\par');
minigraphdata(fid,HICP_GA,'g.a. de l''IPCH');
minigraphdata(fid,TUC,'TUC');
minigraphdata(fid,LoggedR,'$\log R$');
fprintf(fid,'%s\n','\par');

fprintf(fid,'%s\n','\normalsize');
status=fclose(fid)