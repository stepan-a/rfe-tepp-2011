% chargement des s�ries brutes
base

% construction des observables
HICP_GA=100*(log(HICP(5:end-1))-log(HICP(1:end-5)));
dY=100*(log(TPIB0C(5:end-1)./POPTRAV(5:end-1))-log(TPIB0C(4:end-2)./POPTRAV(4:end-2)));
dC=100*(log(MCOF2C(5:end-1)./POPTRAV(5:end-1))-log(MCOF2C(4:end-2)./POPTRAV(4:end-2)));
dI=100*(log(TINV2C(5:end-1)./POPTRAV(5:end-1))-log(TINV2C(4:end-2)./POPTRAV(4:end-2)));
dW=100*(log(TSAL2V(5:end-1)./POPTRAV(5:end-1)./MCOF2P(5:end-1))-log(TSAL2V(4:end-2)./POPTRAV(4:end-2)./MCOF2P(4:end-2)));
LoggedPI=100*log(MCOF2P_HT(5:end-1)./MCOF2P_HT(4:end-2));
LoggedR=100*log(1+TINT3MV(5:end-1)/400);
TUC=TUC(5:end-1);
Lobs=WORKEDHOURS(5:end-1)./POPTRAV(5:end-1);

% on retire le point de 2008Q4
v={'dY' 'dC' 'dI' 'dW' 'HICP_GA' 'LoggedR' 'TUC' 'Lobs'};
for n=1:size(v,2)
    eval([v{n} '=' v{n} '(1:end-1);']);
end