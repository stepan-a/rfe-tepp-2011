function v=dateq(andeb,trimdeb,pas,anfin)

a=floor(andeb:0.25:(anfin+.75))';
q=repmat(['T1';'T2';'T3';'T4'],anfin-andeb+1,1);
v=[int2str(a) q];
v=v(trimdeb:pas:end,3:end);