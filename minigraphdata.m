function minigraphdata(ff,Y,tit,precisiony)

if nargin<4
    precisiony=1;
end

nseries=size(Y,2);


col={'black' 'blue'};
echelle=.3;
largeurencm=10;
hauteurencm=7;

X=1:size(Y,1);
yM=max(max(Y))+0.25*(max(max(Y))-min(min(Y)));
ym=min(min(Y))-0.25*(max(max(Y))-min(min(Y)));
if abs(ym-0)<0.1*(max(max(Y))-min(min(Y)))
    %disp([tit ': correction ym=0']) 
    ym=0;
end

fg=figure();
    subplot(2,2,1)
    plot(X,repmat(yM,1,X(end)));
    hold on
    plot(X,repmat(ym,1,X(end)));
    hold on
    axis tight
    set(gca,'XTick',4:12:X(end))
    set(gca,'XTickLabel',datesq(1996,1,12,2008))
    yt=get(gca,'YTick');
    ytl=get(gca,'YTickLabel');
    yl=get(gca,'YLim');
    xt=get(gca,'XTick');
    xtl=get(gca,'XTickLabel');
    xl=get(gca,'XLim');
close(fg)
    
    ymin=yl(1);
    ymax=yl(end);
    xmin=xl(1);
    xmax=xl(end);
    
    if and(ymin<0,ymax>0)
        axe=1;
    else
        axe=0;
    end
    
	axe=0;

    pas_y=yt(2)-yt(1);
    pas_x=xt(2)-xt(1);
    
    pas_y_cm=hauteurencm/(ymax-ymin)*pas_y;
    pas_x_cm=largeurencm/(xmax-xmin)*pas_x;
    

    ncmPremTicky=(yt(1)-ymin)/pas_y*pas_y_cm;
    ncmPremTickx=(xt(1)-xmin)/pas_x*pas_x_cm;

    nbTicky=size(yt,2);
    nbTickx=size(xt,2);

    ytik=ncmPremTicky+[0; cumsum(repmat(pas_y_cm,nbTicky-1,1))];
    xtik=ncmPremTickx+[0; cumsum(repmat(pas_x_cm,nbTickx-1,1))];
    
    yl=num2str(str2num(ytl),['%3.' int2str(precisiony) 'f']);
    xl=xtl;%num2str(str2num(xtl),'%3.2f');

    
    fprintf(ff,'%s\n',['\begin{tikzpicture}[scale=' num2str(echelle) ']']);
    fprintf(ff,'%s\n',['\coordinate (y) at (0,' num2str(hauteurencm*1.05) ');']);
    fprintf(ff,'%s\n',['\coordinate (x) at (' num2str(largeurencm*1.05) ',0);']);
    
Ycm = (Y-ymin)/pas_y*pas_y_cm;
Xcm = (X-xmin)/pas_x*pas_x_cm;

if axe
    fprintf(ff,'%s\n','\draw[very thin,red]');
    fprintf(ff,'%s\n',['(0 ,' num2str(-ymin/pas_y*pas_y_cm,'%7.4f') ') -- (' num2str(Xcm(end),'%7.4f') ',' num2str(-ymin/pas_y*pas_y_cm,'%7.4f') ');']) ;
end

    
fprintf(ff,'%s\n','\draw[thin,red,densely dashed]');
fprintf(ff,'%s\n',['(0 ,' num2str(mean(Ycm),'%7.4f') ') -- (' num2str(Xcm(end),'%7.4f') ',' num2str(mean(Ycm),'%7.4f') ');']) ;

    
    fprintf(ff,'%s','\foreach \x/\xtext in {');
    compteur=1;
    for x=xtik(1:end-1)'
        fprintf(ff,'%s',[num2str(x,'%3.2f') '/' xl(compteur,:) ',']);
        compteur=compteur+1;
    end
    fprintf(ff,'%s\n',[num2str(xtik(end),'%3.2f') '/' xl(compteur,:) '}']);
    fprintf(ff,'%s\n','\draw (\x cm,1pt) -- (\x cm,-3pt)');
    fprintf(ff,'%s\n','node[below] {\xtext};');

    fprintf(ff,'%s',['\foreach \y/\ytext in {']);
    compteur=1;
    for y=ytik(1:end-1)'
        fprintf(ff,'%s',[num2str(y,'%3.2f') '/' yl(compteur,:) ',']);
        compteur=compteur+1;
    end
    fprintf(ff,'%s\n',[num2str(ytik(end),'%3.2f')  '/' yl(compteur,:) '}']);
    fprintf(ff,'%s\n','\draw (1pt,\y cm) -- (-3pt,\y cm) node[anchor=east] {\ytext};');
    
    fprintf(ff,'%s\n','\draw[<->] (y) node[above] {} -- (0,0) --  (x) node[right] {};');
    %fprintf(ff,'%s\n',['\draw[-] (y) -- (' num2str(largeurencm) ',' num2str(hauteurencm) ') --  (x) ;']);



    %    fprintf(ff,'%s\n','\node[below=0.3cm,] at (6,0) {quarters};');
    %    fprintf(ff,'%s\n','\node[left=0.7cm,rotate=90] at (0,9) {percentage points};');


for k=1:nseries
    fprintf(ff,'%s\n',['\draw[thick,' col{k} ']']);
    for i =X(1:end-1)
        if ~isnan(Ycm(i,k))
            fprintf(ff,'%s\n',['(' num2str(Xcm(i),'%7.4f') ',' num2str(Ycm(i,k),'%7.4f') ') --']) ;
        end
    end
    if isnan(Ycm(end,k))
        fprintf(ff,'%s\n',['(' num2str(Xcm(end-1),'%7.4f') ',' num2str(Ycm(end-1,k),'%7.4f') ');']) ;
    else
        fprintf(ff,'%s\n',['(' num2str(Xcm(end),'%7.4f') ',' num2str(Ycm(end,k),'%7.4f') ');']) ;
    end
end
    fprintf(ff,'%s\n',['\node at (' num2str(largeurencm/2) ',' num2str(hauteurencm*1.05) ') { \scriptsize ' tit '};']);
    
    fprintf(ff,'%s\n','\draw[thin,white] (-1.5,0);');
    %fprintf(ff,'%s\n',['\draw[thin,white] (' num2str(largeurencm+2) ',0);']);
    fprintf(ff,'%s\n',['\draw[thin,white] (0,' num2str(hauteurencm+1.5) ');']);
    fprintf(ff,'%s\n','\end{tikzpicture}');

end


